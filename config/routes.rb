require 'sidekiq/web'

Rails.application.routes.draw do

  get 'user/index'

  get 'user/new'

  get 'user/create'

  devise_for :users, controllers: {
    sessions: 'users/sessions'
  }

  mount Sidekiq::Web => '/sidekiq'

  scope "/admin" do
    resources :users
  end

  root 'weddings#dashboard'

  resources :events
  resources :settings do
    member do
      put :aktifasi
      patch :customtext
    end
    collection do
      get :reset
    end
  end
  resources :displays
  resources :type_of_events
  resources :guests do
  	collection {
    post :import
    get :presence
    get :transfer
    get :gift
    get :print_qr
    get :print_no_undian
  	}
  end

  resources :weddings do
    collection{
      get :update_presence
      get :printulang
      get :reprint
      get :update_souvenir
      get :print
      get :print_qr
      get :print_qr_master
      get :template_qr
      get :template_qr_master
      get :report_wedding
      get :souvenir
      get :export_excel
      get :print_qr_page
      get :report
      get :dashboard
      get :search
      post :import  
    }
  end

  resources :gatherings do
    collection{
      post :import
      get :update_presence
      get :printulang
      get :reprint
      get :update_souvenir
      get :print
      get :print_qr
      get :print_qr_master
      get :template_qr
      get :template_qr_master
      get :souvenir
      get :export_excel
      get :print_qr_page
      get :report
      get :dashboard
      get :search
      get :pengaturan
      get :reset
    }
  end

  resources :concerts do
    collection{
      post :import
      get :update_presence
      get :printulang
      get :reprint
      get :update_souvenir
      get :print
      get :print_qr
      get :print_qr_master
      get :template_qr
      get :template_qr_master
      get :report_concert
      get :souvenir
      get :export_excel
      get :print_qr_page
      get :report
      get :dashboard
      get :search
      get :pengaturan
      get :reset
    }
  end

  get '/dashboards/coming_soon', to: 'dashboards#coming_soon'
  get '/dashboards', to: 'dashboards#index'

end
