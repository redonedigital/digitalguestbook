class Concert < ApplicationRecord
  belongs_to :event

    def self.import(file,event_id)
			spreadsheet = Roo::Spreadsheet.open(file.path)
      header = spreadsheet.row(1)
      (2..spreadsheet.last_row).each do |i|
        row = Hash[[header, spreadsheet.row(i)].transpose]
        row["event_id"] = event_id
        concert = find_by_id(row["id"]) || new
        concert.attributes = row.to_hash
        concert.save!
      end
    end

end
