class Gathering < ApplicationRecord
	belongs_to :event, dependent: :destroy

	def self.search(name)
    name = name ? name.upcase : ""
    where("upper(nama_peserta) ilike ?", "%#{name}%")
	end
	
  def self.import(file,event_id)
		spreadsheet = Roo::Spreadsheet.open(file.path)
		header = spreadsheet.row(1)
		(2..spreadsheet.last_row).each do |i|
		  row = Hash[[header, spreadsheet.row(i)].transpose]
		  row["event_id"] = event_id
			gathering = find_by_id(row["id"]) || new
		  gathering.attributes = row.to_hash
		  gathering.save!
		end
	end

	def name_with_initial
    "#{nama_peserta} / #{unit_kerja} / #{nip}"
	end
	
end
