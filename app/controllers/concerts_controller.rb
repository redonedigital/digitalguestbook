class ConcertsController < ApplicationController
  include PresencesHelper

  def index
    render layout: 'presence'
  end

  def new
    if params[:search].present?
      data = JSON.parse(params[:search])["data_search"]
      if data["data"] == "all"
        @concerts = Concert.joins(event: :type_of_event).where("events.status = TRUE")
                      .order(nama: :asc)
                      .page(params[:page]).per(100)
      else
        @concerts = search_concert(data["name"],data["alamat"],data["kategori"])
      end
      respond_to do |f|
        f.js
      end
    else
      @page = params[:page]


      @concerts = Concert.joins(event: :type_of_event).where("events.status = TRUE")
                      .order(nama: :asc)
                      .page(@page).per(5)

      respond_to do |f|
        f.js
        f.html
      end
    end
  end

  # GET /guests/import
  def import
    Concert.import(params[:file],params[:concert][:event_id])
    redirect_to concerts_path, notice: "Concert guest imported"
  end

  def pengaturan
  end

  def reset
    event_id = Event.find_by(status: true).id
    concert = Concert.where(event_id: event_id)
    concert.update_all(:presence => false, :time_of_entry => nil)
    redirect_to concerts_path, notice: "Concert reset"
  end

  def print
    render layout: false
  end

  def template_qr
    render layout: false
  end

  def template_qr_master
    render layout: false
  end

  def print_qr
    if params[:guest_qr].present?
      find_guest = Concert.where("name ilike '%#{params[:guest_qr]}%'").limit(1)
      @qrcode = find_guest.first.guest_id.to_s
    end

    respond_to do |format|
      format.js { render 'concerts/print_qr' }
    end
  end

  def print_qr_master
    if params[:guest_id].present?
      @find_guest = params[:guest_id]
    end

    respond_to do |format|
      format.js { render 'concerts/print_qr_master' }
    end
  end

  def update_presence
    @find_guest = Concert.where(guest_id: params[:guest_code])

    if @find_guest.present? && @find_guest.first.presence != TRUE
      setting = Setting.where(id:@find_guest.first.event_id)
      @settingan = Setting.last
      @print = {}
      ScanWorker.perform_async(params[:guest_code])
      respond_to do |format|
        format.js { render 'concerts/update_presence' }
      end
    else
      @presence = if @find_guest.first.presence == TRUE
        TRUE
      else
        FALSE
      end
      respond_to do |format|
        format.js { render 'concerts/index' }
      end
    end
  end

  def printulang
    render layout: 'presence'
  end

  def reprint
    @find_guest = Concert.where(guest_id: params[:guest_code])

    if @find_guest.present? && @find_guest.first.presence == TRUE
      setting = Setting.where(id:@find_guest.first.event_id)
      @print = {}
      respond_to do |format|
        format.js { render 'concerts/reprint' }
      end
    else
      respond_to do |format|
        format.js { render 'concerts/index' }
      end
    end
  end

  def create
    @concert = Concert.new(guest_id:params)
    
    respond_to do |format|
      if @concert.save
        format.html { redirect_to @concert, notice: 'Concert was successfully created.' }
        format.json { render :show, status: :created, location: @concert }
      else
        format.html { render :new }
        format.json { render json: @concert.errors, status: :unprocessable_entity }
      end
    end
  end

  def souvenir
    render layout: 'presence'
  end

  def update_souvenir
    @find_guest = Concert.where(guest_id: params[:souvenir])
    if @find_guest.present? && @find_guest.first.ticket != TRUE
      @souvenir = 'Sukses'
      SouvenirWorker.perform_async(params[:souvenir])
    elsif @find_guest.present? && @find_guest.first.ticket == TRUE
      @souvenir = 'Sudah'
    elsif @find_guest.blank?
      @souvenir = 'Gagal'
    end

    respond_to do |format|
      format.js { render 'concerts/index' }
    end
  end

  def print_qr_page
    render layout: 'presence'
  end

  def update_print_qr_page
  end

  def dashboard
    content = {}
    @total_guest = Concert.joins(:event).where("events.status=true").count
    @guest_sudah_hadir = Concert.joins(:event).where("events.status=true").where("concerts.presence=true").count 
    @guest_belum_hadir = Concert.joins(:event).where("events.status=true").where("concerts.presence=false").count
    @jumlah_souvenir = Concert.joins(:event).where("events.status=true").where("concerts.ticket=false").count
  end

  def report
    @setting = Setting.first
    @events = TypeOfEvent.joins(:events).where(name:"Concert").select("events.name as name,events.id as id")
    @concerts = Concert.where(event_id:params[:event_id]).order(guest_id: :asc)

    respond_to do |format|
      format.html
      format.xlsx
    end
  end

  def report_concert
    unless params[:event].nil?
      if params[:jenis_kategori].present?
        data_hadir = Concert.joins(:event)
                           .where("events.name like '%#{params[:event][:event_id]}%'")
                          .where(presence:true)
        data_tidak_hadir = Concert.joins(:event)
                                .where("events.name like '%#{params[:event][:event_id]}%'")
                                .where('guests.presence = false or guests.presence = null')
        @report = tentukan_jenis_report(params[:jenis_kategori],data_hadir,data_tidak_hadir,params[:presence_kategori])
      end

      @no = if params[:page].to_i == 1 || params[:page].blank?
        1
      else
        (params[:page].to_i * 100) - 100 + 1
      end

      respond_to do |format|
        format.js { render '/concerts/report_wedding' }
      end
    else
      @jenis_kategori = delete_duplicate_value(Concert.select(:category))
    end
  end

  def export_excel
    @custom_header = Setting.all
    @exports= Concert.joins(:event).where("events.name like '%#{params[:event]}%'")
    respond_to do |format|
      format.xls
    end
  end

end
