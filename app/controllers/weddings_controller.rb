class WeddingsController < ApplicationController
  include PresencesHelper
  
  def import
    authorize! :create, Guest
    result = Guest.import(params[:file],params[:guest][:event_id])
    @r = 0 if result == false
    respond_to do |f|
      f.js 
    end
  end

  def index
    render layout: 'presence'
  end

  def new
    if params[:search].present?
      data = JSON.parse(params[:search])["data_search"]
      if data["data"] == "all"
        @weddings = Guest.joins(event: :type_of_event).where("type_of_events.id = 1").where("type_of_events.name ilike '%Wedding%'").where("events.status = TRUE")
                      .order(nama: :asc)
                      .page(params[:page]).per(100)
      else
        @weddings = search(data["name"],data["alamat"],data["kategori"])
      end
      respond_to do |f|
        f.js
      end
    else
      @weddings = Guest.joins(event: :type_of_event).where("type_of_events.id = 1").where("type_of_events.name ilike '%Wedding%'").where("events.status = TRUE")
                      .order(nama: :asc)
                      .page(params[:page]).per(100)
      respond_to do |f|
        f.js
        f.html
      end
    end
  end

  def print
    render layout: false
  end

  def template_qr
    render layout: false
  end

  def template_qr_master
    render layout: false
    @find_guest = Guest.where(guest_id: params[:guest_id])
  end

  def print_qr
    if params[:guest_qr].present?
      find_guest = Guest.where("nama ilike '%#{params[:guest_qr]}%'").limit(1)
      @qrcode = find_guest.first.guest_id.to_s
    end

    ScanWorker.perform_async(find_guest.first.guest_id)

    respond_to do |format|
      format.js { render 'weddings/print_qr' }
    end
  end

  def print_qr_master
    if params[:guest_id].present?
      @find_guest = params[:guest_id]
    end

    respond_to do |format|
      format.js { render 'weddings/print_qr_master' }
    end
  end

  def update_presence
    @find_guest = Guest.where(guest_id: params[:guest_code])
    @display = Display.find(1)
    if @find_guest.present? && @find_guest.first.presence != true
      setting = Setting.where(id:@find_guest.first.event_id)
      @settingan = Setting.last
      @print = {}
      ScanWorker.perform_async(params[:guest_code])
      
      @display = Display.find(1)      

      @dis_array = []
      i = 1
       
      while i <= 13  do
        @dis_array.push("alamat") if i==1 && @display.alamat == true  
        @dis_array.push("nomor_ponsel") if i==2 && @display.nomor_ponsel == true  
        @dis_array.push("kategori") if i==3 && @display.kategori == true  
        @dis_array.push("status") if i==4 && @display.status == true  

        @dis_array.push("nama_meja") if i==5 && @display.nama_meja == true  
        @dis_array.push("guest_id") if i==6 && @display.guest_id == true  
        @dis_array.push("souvenir") if i==7 && @display.souvenir == true  
        @dis_array.push("jumlah_souvenir") if i==8 && @display.jumlah_souvenir == true  

        @dis_array.push("nama_angpao") if i==9 && @display.nama_angpao == true  
        @dis_array.push("no_undian") if i==10 && @display.no_undian == true 
        @dis_array.push("jumlah_undangan") if i==11 && @display.jumlah_undangan == true 
        @dis_array.push("jabatan") if i==12 && @display.jabatan == true 
        i += 1
      end


      respond_to do |format|
        format.js { render 'weddings/update_presence' }
      end
    else
      @presence = if @find_guest.first.presence == true
        true
      else
        false
      end
      respond_to do |format|
        format.js { render 'weddings/index' }
      end
    end
  end

  def printulang
    render layout: 'presence'
  end

  def reprint
    @find_guest = Guest.where(guest_id: params[:guest_code])

    if @find_guest.present? && @find_guest.first.presence == TRUE
      setting = Setting.where(id:@find_guest.first.event_id)
      @print = {}
      respond_to do |format|
        format.js { render 'weddings/reprint' }
      end
    else
      respond_to do |format|
        format.js { render 'weddings/index' }
      end
    end
  end

  def create
    guest = Guest.new
    guest.name = params[:name]
    guest.city = params[:kota]
    guest.address = params[:address]
    guest.telephone = params[:telephone]
    guest.category = params[:category]
    guest.event_id = Event.joins(:type_of_event)
                          .where(status:true)
                          .where("type_of_events.id = 1")
                          .where("type_of_events.name ilike '%Wedding%'").first.id
    # guest.amount_of_present = params[:amount_of_present]
    guest.type_of_guest = params[:type_of_guest]
    guest.presence = true
    guest.nama_meja = params[:nama_meja]
    # guest.no_undian =  rand.to_s.slice(2,5)
    guest.time_of_entry = Time.now()
    guest.guest_id = Guest.generate_number(guest.name)
    guest.save
    respond_to do |format|
      format.html { redirect_to new_wedding_path }
      format.js
    end
  end

  def souvenir
    render layout: 'presence'
  end

  def update_souvenir
    @find_guest = Guest.where(guest_id: params[:souvenir])
    if @find_guest.present? && @find_guest.first.souvenir != TRUE
      @souvenir = 'Sukses'
      SouvenirWorker.perform_async(params[:souvenir])
    elsif @find_guest.present? && @find_guest.first.souvenir == TRUE
      @souvenir = 'Sudah'
    elsif @find_guest.blank?
      @souvenir = 'Gagal'
    end

    respond_to do |format|
      format.js { render 'weddings/index' }
    end
  end

  def print_qr_page
    render layout: 'presence'
  end

  def update_print_qr_page
  end

  def dashboard
    content = {}
    @total_guest = Guest.joins(:event).where("events.status=true").count
    @guest_sudah_hadir = Guest.joins(:event).where("events.status=true").where("guests.presence=true").count
    @guest_belum_hadir = Guest.joins(:event).where("events.status=true").where("guests.presence=false").count
    @jumlah_souvenir = Guest.joins(:event).where("events.status=true").where("guests.souvenir=true").count
  end

  def report
    @setting = Setting.first
    @events = TypeOfEvent.joins(:events).where(name:"Wedding").select("events.name as name,events.id as id")
    @jenis_kategori = delete_duplicate_value(Guest.select(:kategori))
    if params[:event_id].present? && params[:jenis_kategori] == 'all'
      query = "event_id = #{params[:event_id]}"
    elsif params[:event_id].present? && params[:jenis_kategori] != 'all'
      query = "event_id = #{params[:event_id]} AND category ilike '%#{params[:jenis_kategori]}%'"
    end
    @guests = Guest.where(query)

    respond_to do |format|
      format.html
      format.xlsx
    end
  end

  def report_wedding
    unless params[:event].nil?
      if params[:jenis_kategori].present?
        data_hadir = Guest.joins(:event)
                           .where("events.name like '%#{params[:event][:event_id]}%'")
                          .where(presence:true)
        data_tidak_hadir = Guest.joins(:event)
                                .where("events.name like '%#{params[:event][:event_id]}%'")
                                .where('guests.presence = false or guests.presence = null')
        @report = tentukan_jenis_report(params[:jenis_kategori],data_hadir,data_tidak_hadir,params[:presence_kategori])
      end

      @no = if params[:page].to_i == 1 || params[:page].blank?
        1
      else
        (params[:page].to_i * 100) - 100 + 1
      end

      respond_to do |format|
        format.js { render '/weddings/report_wedding' }
      end
    else
      @jenis_kategori = delete_duplicate_value(Guest.select(:category))
    end
  end

  def export_excel
    @custom_header = Setting.all
    @exports= Guest.joins(:event).where("events.name like '%#{params[:event]}%'")
    respond_to do |format|
      format.xls
    end
  end

end
