class GuestsController < ApplicationController
  before_action :set_guest, only: [:show, :edit, :update, :destroy]
  include GuestsHelper
  layout 'application', :except => :print_no_undian


  # GET /guests
  # GET /guests.json
  def index
    @guests = GuestTemporary.all
    @guests1 = Guest.search(params[:name]).order(:nama).page(params[:page]).per(10)
  end

  # GET /guests/1
  # GET /guests/1.json
  def show
  end

  # GET /guests/new
  def new
    @guest = Guest.new
  end

  # GET /guests/1/edit
  def edit
  end

  # POST /guests
  # POST /guests.json
  def create
    @guest = Guest.new(guest_params)
    @guest.guest_id = Guest.generate_number(@guest.name)

    respond_to do |format|
      if @guest.save
        format.html { redirect_to @guest, notice: 'Guest was successfully created.' }
        format.json { render :show, status: :created, location: @guest }
      else
        format.html { render :new }
        format.json { render json: @guest.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /guests/1
  # PATCH/PUT /guests/1.json
  def update
    respond_to do |format|
      if @guest.update(guest_params)
        format.html { redirect_to @guest, notice: 'Guest was successfully updated.' }
        format.json { render :show, status: :ok, location: @guest }
      else
        format.html { render :edit }
        format.json { render json: @guest.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /guests/1
  # DELETE /guests/1.json
  def destroy
    @guest.destroy
    respond_to do |format|
      format.html { redirect_to guests_url, notice: 'Guest was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # GET /guests/import
  def import
    Guest.import(
      params[:file],
      params[:guest][:event_id],
    )
    redirect_to guests_path, notice: "Guests imported"
  end

  # GET /guests/presence
  def presence
    # if params[:guest_code].present?
    #   find_guest = Guest.where(guest_id: params[:guest_code])
    #   find_guest.update(presence:true)
    #   # @qrcode = RQRCode::QRCode.new(find_guest.first.no_undian.to_s,size: 0.1)
    #   # @png = @qrcode.as_png(
    #   #     resize_gte_to: false,
    #   #     resize_exactly_to: false,
    #   #     fill: 'white',
    #   #     color: 'black',
    #   #     size: 120,
    #   #     border_modules: 4,
    #   #     module_px_size: 6
    #   # )
    #   # send_data @png, filename: "no_undian_#{find_guest.first.no_undian.to_s}.png", type: @png
    #   redirect_to presence_guests_path, notice: 'ABSEN GUEST SUCCESSFULLY'
    # else
    #   # redirect_to presence_guests_path, notice: 'NOT FOUND'
    #   render presence_guests_path, notice: "NOT_FOUND"
    # end
  end

  def print_no_undian
    begin
      @find_guest = Guest.where(guest_id: params[:guest_code])
      @find_guest.update(presence:true)
    rescue
      render presence_guests_path, notice: "NOT_FOUND"
    end
  end
  
  #GET /guest/print_qr
  def print_qr
    qrcode = RQRCode::QRCode.new(params[:guest_id])
    @png = qrcode.as_png(
        resize_gte_to: false,
        resize_exactly_to: false,
        fill: 'white',
        color: 'black',
        size: 120,
        border_modules: 4,
        module_px_size: 6
    )
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_guest
      @guest = Guest.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def guest_params
      # params.fetch(:guest, {})
      params.require(:guest).permit(:name,:address,:telephone,:presence)
    end
end
