class GatheringsController < ApplicationController
  include GatheringsHelper
  def index
    render layout: 'presence'
  end

  def new
    # @guest = Gathering.new
    if params[:search].present?
      data = JSON.parse(params[:search])["data_search"]
      if data["data"] == "all"
        @gatherings = Gathering.joins(event: :type_of_event).where("event_id = 7")
                      .order(nama_peserta: :asc)
                      .page(params[:page]).per(100)
      else
        @gatherings = Gathering.search(data["name"])
      end
      respond_to do |f|
        f.js
      end
    else
      @gatherings = Gathering.joins(event: :type_of_event).where("event_id = 7")
                      .order(nama_peserta: :asc)
                      .page(params[:page]).per(100)
      respond_to do |f|
        f.html
      end
    end
  end

  # GET /guests/import
  def import
    Gathering.import(params[:file],params[:gathering][:event_id])
    redirect_to gatherings_path, notice: "Gathering guest imported"
  end

  def pengaturan
  end

  def reset
    event_id = Event.find_by(status: true).id
    gathering = Gathering.where(event_id: event_id)
    gathering.update_all(:presence => false, :time_of_entry => nil)
    redirect_to gatherings_path, notice: "Gathering reset"
  end

  def print
    render layout: false
  end

  def template_qr
    render layout: false
  end

  def template_qr_master
    render layout: false
  end

  def print_qr
    if params[:guest_qr].present?
      find_guest = Gathering.where("name ilike '%#{params[:guest_qr]}%'").limit(1)
      @qrcode = find_guest.first.guest_id.to_s
    end

    respond_to do |format|
      format.js { render 'gatherings/print_qr' }
    end
  end

  def print_qr_master
    if params[:guest_id].present?
      @find_guest = params[:guest_id]
    end

    respond_to do |format|
      format.js { render 'gatherings/print_qr_master' }
    end
  end

  def update_presence
    @find_guest = Gathering.where(guest_id: params[:guest_code])

    if @find_guest.present? && @find_guest.first.presence != TRUE
      setting = Setting.where(id:@find_guest.first.event_id)
      @settingan = Setting.last
      @print = {}
      @find_guest.update(presence:true,time_of_entry:Time.now())
      respond_to do |format|
        format.js { render 'gatherings/update_presence' }
      end
    else
      @presence = if @find_guest.first.presence == TRUE
        TRUE
      else
        FALSE
      end
      respond_to do |format|
        format.js { render 'gatherings/index' }
      end
    end


  end

  def printulang
    render layout: 'presence'
  end

  def reprint
    @find_guest = Gathering.where(guest_id: params[:guest_code])

    if @find_guest.present? && @find_guest.first.presence == TRUE
      setting = Setting.where(id:@find_guest.first.event_id)
      @print = {}
      respond_to do |format|
        format.js { render 'gatherings/reprint' }
      end
    else
      respond_to do |format|
        format.js { render 'gatherings/index' }
      end
    end
  end

  def create
    @gathering = Gathering.new(gathering_params)
    
    respond_to do |format|
      if @gathering.save
        format.html { redirect_to @gahering, notice: 'Gathering gathering was successfully created.' }
        format.json { render :show, status: :created, location: @gathering }
      else
        format.html { render :new }
        format.json { render json: @gathering.errors, status: :unprocessable_entity }
      end
    end
  end

  def souvenir
    render layout: 'presence'
  end

  def update_souvenir
    @find_guest = Gathering.where(guest_id: params[:souvenir])
    if @find_guest.present? && @find_guest.first.souvenir != TRUE
      @souvenir = 'Sukses'
      @find_guest.update(souvenir:true,time_of_get_souvenir:Time.now())
    elsif @find_guest.present? && @find_guest.first.souvenir == TRUE
      @souvenir = 'Sudah'
    elsif @find_guest.blank?
      @souvenir = 'Gagal'
    end

    respond_to do |format|
      format.js { render 'gatherings/index' }
    end
  end

  def print_qr_page
    render layout: 'presence'
  end

  def update_print_qr_page
  end

  def dashboard
    content = {}
    @total_guest = Gathering.joins(:event).where("events.status=true").count
    @guest_sudah_hadir = Gathering.joins(:event).where("events.status=true").where("gatherings.presence=true").count 
    @guest_belum_hadir = Gathering.joins(:event).where("events.status=true").where("gatherings.presence=false").count
    @units = Gathering
              .select(" 
                        unit_kerja, 
                        COUNT(*) as Total,
                        COUNT(gatherings.id) filter (where presence = TRUE) as Hadir,
                        COUNT(gatherings.id) filter (where presence = FALSE) as TidakHadir
                      ")
              .group("unit_kerja")
              .order(unit_kerja: :asc)
  end

def report
  @setting = Setting.first
  @events = TypeOfEvent.joins(:events).where(name:"Gathering").select("events.name as name,events.id as id")
  @jenis_kategori = delete_duplicate_value(Gathering.select(:unit_kerja))
  if params[:event_id].present? && params[:jenis_kategori] == 'all'
    query = "event_id = #{params[:event_id]}"
  elsif params[:event_id].present? && params[:jenis_kategori] != 'all'
    query = "event_id = #{params[:event_id]} AND unit_kerja ilike '%#{params[:jenis_kategori]}%'"
  end
  @guests = Gathering.where(query).order(guest_id: :asc)

  respond_to do |format|
    format.html
    format.xlsx
  end

  # unless params[:event].nil?
  #   if params[:jenis_kategori].present?
  #     data_hadir = Gathering.joins(:event)
  #                        .where("events.name like '%#{params[:event][:event_id]}%'")
  #                       .where(presence:true)
  #     data_tidak_hadir = Gathering.joins(:event)
  #                             .where("events.name like '%#{params[:event][:event_id]}%'")
  #                             .where('guests.presence = false or guests.presence = null')
  #     @report = tentukan_jenis_report(params[:jenis_kategori],data_hadir,data_tidak_hadir,params[:presence_kategori])
  #     respond_to do |format|
  #       format.html
  #     end
  #   end
  # end
end

  def report_wedding
    unless params[:event].nil?
      if params[:jenis_kategori].present?
        data_hadir = Gathering.joins(:event)
                           .where("events.name like '%#{params[:event][:event_id]}%'")
                          .where(presence:true)
        data_tidak_hadir = Gathering.joins(:event)
                                .where("events.name like '%#{params[:event][:event_id]}%'")
                                .where('guests.presence = false or guests.presence = null')
        @report = tentukan_jenis_report(params[:jenis_kategori],data_hadir,data_tidak_hadir,params[:presence_kategori])
      end

      @no = if params[:page].to_i == 1 || params[:page].blank?
        1
      else
        (params[:page].to_i * 100) - 100 + 1
      end

      respond_to do |format|
        format.js { render '/gatherings/report_wedding' }
      end
    else
      @jenis_kategori = delete_duplicate_value(Gathering.select(:category))
    end
  end

  def export_excel
    @custom_header = Setting.all
    @exports= Gathering.joins(:event).where("events.name like '%#{params[:event]}%'")
    respond_to do |format|
      format.xls
    end
  end

end
