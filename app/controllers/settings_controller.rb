class SettingsController < ApplicationController
  skip_before_action :verify_authenticity_token
  include SettingsHelper

  def index
    @event_active = Event.where('status = true').first
    @events = Event.where('status = false').order(:name)
    @setting = Setting.first
    @check_box_settings = Setting.first
    @display = Display.joins(:setting).first
  end

  def aktifasi
    @event = Event.find(params[:id])
    @event_lama = Event.where('status = true')
    ActiveRecord::Base.transaction do
      @event_lama.update(status:false)
      @event.update(status:true)
    end
    redirect_to settings_path, notice: 'Event berhasil diupdate'
  end

  def update
    @setting = Setting.find(1)
    begin
      if params[:no_print].present?
        @setting.update_attributes(
          no_undian:false,
          nama_meja:false,
          nama_undangan:false,
          jumlah_souvenir:false,
          nama_angpao:false
        )
      else
        @setting.update_attributes(
          no_undian:params[:setting][:no_undian].present? ? true : false,
          nama_meja:params[:setting][:nama_meja].present? ? true : false,
          nama_undangan:params[:setting][:nama_undangan].present? ? true : false,
          jumlah_souvenir:params[:setting][:jumlah_souvenir].present? ? true : false,
          nama_angpao:params[:setting][:nama_angpao].present? ? true : false,
          custom_one_text:params[:setting][:custom_one_text].present? ? params[:setting][:custom_one_text] : nil 
        )
      end
      @con = true
      respond_to do |format|
        format.js { render 'settings/index' }
      end
      
    rescue
      @con = false
      respond_to do |format|
        format.js { render 'settings/index' }
      end
    end
  
  end

  def customtext
    @setting = Setting.find(1)
    if @setting.update(customtext_params)
      # Guest.joins(:event).where("events.status = true").update(guest_params)
      redirect_to settings_path, notice: 'Currency was successfuly edited.'
    end
  end

  def reset
    event_id = Event.find_by(status: true).id
    guest = Gathering.where(event_id: event_id)
    guest.update_all(:presence => false, :time_of_entry => nil,:ticket => false, :time_of_get_ticket => nil)
  end

  private

  def customtext_params
    params.require(:setting).permit(:custom_one_text,:custom_two_text,:custom_three_text,:custom_four_text,:custom_five_text)
  end

end
