class ScanWorker
  include Sidekiq::Worker
  
  def perform(guest_id)
    guest = Guest.where(guest_id: guest_id)
    guest.update(presence:true,time_of_entry:Time.now())
  end
end
