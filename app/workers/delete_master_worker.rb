class DeleteMasterWorker
  include Sidekiq::Worker

  def perform(id)
  	Event.find(id).destroy
    # Do something
  end
end
