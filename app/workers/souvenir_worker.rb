class SouvenirWorker
  include Sidekiq::Worker

  def perform(guest_id)
    souvenir = Guest.where(guest_id: guest_id)
    # souvenir.update(ticket:true,time_of_get_ticket:Time.now())
    souvenir.update(souvenir:true,time_of_get_souvenir:Time.now())
  end
end
